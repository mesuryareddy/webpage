# app.py

from flask import Flask, render_template, redirect, url_for, request, session, flash
from pymongo import MongoClient
from werkzeug.security import generate_password_hash, check_password_hash
from bson.binary import Binary

app = Flask(__name__)
app.secret_key = 'your_secret_key'

# MongoDB configuration
client = MongoClient('mongodb://localhost:27017/')
db = client['pywebpage']
users_collection = db['users']
uploads_collection = db['uploads']

@app.route('/')
def index():
    if 'email' in session:
        email = session['email']
        return render_template('index.html', email=email)
    return redirect(url_for('login'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        user = users_collection.find_one({'email': email})
        
        if user and check_password_hash(user['password'], password):
            session['email'] = email
            return redirect(url_for('index'))
        else:
            flash('Invalid email or password')
    
    return render_template('login.html')

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        hashed_password = generate_password_hash(password)
        
        if users_collection.find_one({'email': email}):
            flash('Email address already exists')
            return redirect(url_for('register'))
        
        users_collection.insert_one({'email': email, 'password': hashed_password})
        
        session['email'] = email
        return redirect(url_for('index'))
    
    return render_template('register.html')

@app.route('/logout')
def logout():
    session.pop('email', None)
    return redirect(url_for('login'))

@app.route('/upload', methods=['POST'])
def upload_file():
    if 'email' not in session:
        flash('Please log in to upload files')
        return redirect(url_for('login'))
    
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file:
        filename = file.filename
        file_data = Binary(file.read())
        uploads_collection.insert_one({'filename': filename, 'data': file_data})
        flash('File uploaded successfully')
        return redirect(request.url)

if __name__ == '__main__':
    app.run(debug=True)